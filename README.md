# do-zero-ao-deploy

Projeto para desenvolvimento e deploy utilizando apenas Lektor, BDD e GitLab CI.
Vídeos do Tutorial:


##### Aula 0 `https://www.youtube.com/watch?v=LpWPijZYWnQ`
- Apresentação Projeto

##### Aula 1 `https://www.youtube.com/watch?v=wDjZGkfphbk`
- Deploy no gitlabpages

##### Aula 3 `https://www.youtube.com/watch?v=L69ZBHIqPZo`
- Configuração de ambiente


###### Link do Projeto
`https://serrones.gitlab.io/do-zero-ao-deploy/`
